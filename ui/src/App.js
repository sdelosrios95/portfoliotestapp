import logo from "./logo.svg";
import React from 'react'

import Avatar from "./components/avatar/avatar";
import Timeline from "./components/timeline/timeline";
import Experience from "./components/experience/experience";

import { getUserPortfolio } from './services/portfolio/portfolio'

import "./App.css";

export default class App extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      avatarUrl: '',
      firstName: '',
      lastName: '',
      title: '',
      tweets: [],
    }
  }

  async componentDidMount() {
    const userPortfolio = await getUserPortfolio('1')

    this.setState({
      avatarUrl: userPortfolio.imageUrl,
      firstName: userPortfolio.firstName,
      lastName: userPortfolio.lastName,
      tweets: userPortfolio.portfolioTweets,
      description: userPortfolio.description,
    })
    
  }

  render() {
    return (
      <div className="App">
      <header className="App-header">
        <h1>Portfolio Zemoga test</h1>
      </header>
      <div className="App-content">
        <div className="left-panel">
          <Avatar imgSrc={this.state.avatarUrl} />
          <Timeline firstName={this.state.firstName} tweets={this.state.tweets} />
        </div>
        <div className="right-panel">
          <Experience
            fullName={`${this.state.firstName} ${this.state.lastName}`}
            experienceTitle={"My Work experience"}
          >
            <p>{this.state.description}</p>
          </Experience>
        </div>
      </div>
    </div>
    )
  }
}